<!DOCTYPE HTML>
<html>

@include('partials.head')

<body>
	@include('partials.header.header')

	<!-- PORTFOLIO -->

	<div id="wrapper-container">

		@section('content1')
		@show

		@include('partials.footer')

	</div>



	@include('partials.script')


</body>


</html>
