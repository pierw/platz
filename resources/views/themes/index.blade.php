
@foreach ($themes as $theme)

  <div id="bouton-ai">
    <img src="{{ Voyager::image((json_decode($theme->image))[0]->download_link) }}" alt="{{ $theme->name }}" title="{{ $theme->name }}" height="28" width="28">
  </div>

@endforeach
