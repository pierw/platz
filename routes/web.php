<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PostController@homepage')->name('homepage');


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

Route::prefix('posts')->name('posts.')->group(function () {
    Route::get('/', 'PostController@index')->name('index');
    Route::get('/{id}', 'PostController@show')->name('show');
});
