<?php

use Illuminate\Database\Seeder;

class MenuItemsTableCustomizationsSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


      \DB::statement('SET FOREIGN_KEY_CHECKS=0;');
      \DB::table('menu_items')->delete();
      \DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        \DB::table('menu_items')->insert(array (
            0 =>
            array (
                'id' => 1,
                'menu_id' => 1,
                'title' => 'Dashboard',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-boat',
                'color' => NULL,
                'parent_id' => NULL,
                'order' => 1,
                'created_at' => '2019-05-04 17:06:34',
                'updated_at' => '2019-05-04 17:06:34',
                'route' => 'voyager.dashboard',
                'parameters' => NULL,
            ),
            1 =>
            array (
                'id' => 2,
                'menu_id' => 1,
                'title' => 'Media',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-images',
                'color' => NULL,
                'parent_id' => NULL,
                'order' => 5,
                'created_at' => '2019-05-04 17:06:34',
                'updated_at' => '2019-05-04 17:06:34',
                'route' => 'voyager.media.index',
                'parameters' => NULL,
            ),
            2 =>
            array (
                'id' => 3,
                'menu_id' => 1,
                'title' => 'Users',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-person',
                'color' => NULL,
                'parent_id' => NULL,
                'order' => 3,
                'created_at' => '2019-05-04 17:06:34',
                'updated_at' => '2019-05-04 17:06:34',
                'route' => 'voyager.users.index',
                'parameters' => NULL,
            ),
            3 =>
            array (
                'id' => 4,
                'menu_id' => 1,
                'title' => 'Roles',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-lock',
                'color' => NULL,
                'parent_id' => NULL,
                'order' => 2,
                'created_at' => '2019-05-04 17:06:34',
                'updated_at' => '2019-05-04 17:06:34',
                'route' => 'voyager.roles.index',
                'parameters' => NULL,
            ),
            4 =>
            array (
                'id' => 5,
                'menu_id' => 1,
                'title' => 'Tools',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-tools',
                'color' => NULL,
                'parent_id' => NULL,
                'order' => 9,
                'created_at' => '2019-05-04 17:06:34',
                'updated_at' => '2019-05-04 17:06:34',
                'route' => NULL,
                'parameters' => NULL,
            ),
            5 =>
            array (
                'id' => 6,
                'menu_id' => 1,
                'title' => 'Menu Builder',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-list',
                'color' => NULL,
                'parent_id' => 5,
                'order' => 10,
                'created_at' => '2019-05-04 17:06:34',
                'updated_at' => '2019-05-04 17:06:34',
                'route' => 'voyager.menus.index',
                'parameters' => NULL,
            ),
            6 =>
            array (
                'id' => 7,
                'menu_id' => 1,
                'title' => 'Database',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-data',
                'color' => NULL,
                'parent_id' => 5,
                'order' => 11,
                'created_at' => '2019-05-04 17:06:34',
                'updated_at' => '2019-05-04 17:06:34',
                'route' => 'voyager.database.index',
                'parameters' => NULL,
            ),
            7 =>
            array (
                'id' => 8,
                'menu_id' => 1,
                'title' => 'Compass',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-compass',
                'color' => NULL,
                'parent_id' => 5,
                'order' => 12,
                'created_at' => '2019-05-04 17:06:34',
                'updated_at' => '2019-05-04 17:06:34',
                'route' => 'voyager.compass.index',
                'parameters' => NULL,
            ),
            8 =>
            array (
                'id' => 9,
                'menu_id' => 1,
                'title' => 'BREAD',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-bread',
                'color' => NULL,
                'parent_id' => 5,
                'order' => 13,
                'created_at' => '2019-05-04 17:06:34',
                'updated_at' => '2019-05-04 17:06:34',
                'route' => 'voyager.bread.index',
                'parameters' => NULL,
            ),
            9 =>
            array (
                'id' => 10,
                'menu_id' => 1,
                'title' => 'Settings',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-settings',
                'color' => NULL,
                'parent_id' => NULL,
                'order' => 14,
                'created_at' => '2019-05-04 17:06:34',
                'updated_at' => '2019-05-04 17:06:34',
                'route' => 'voyager.settings.index',
                'parameters' => NULL,
            ),
            10 =>
            array (
                'id' => 11,
                'menu_id' => 1,
                'title' => 'Hooks',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-hook',
                'color' => NULL,
                'parent_id' => 5,
                'order' => 13,
                'created_at' => '2019-05-04 17:06:34',
                'updated_at' => '2019-05-04 17:06:34',
                'route' => 'voyager.hooks',
                'parameters' => NULL,
            ),
            11 =>
            array (
                'id' => 12,
                'menu_id' => 1,
                'title' => 'Posts',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-news',
                'color' => NULL,
                'parent_id' => NULL,
                'order' => 15,
                'created_at' => '2019-05-06 07:28:01',
                'updated_at' => '2019-05-06 07:28:01',
                'route' => 'voyager.posts.index',
                'parameters' => NULL,
            ),
            12 =>
            array (
                'id' => 13,
                'menu_id' => 1,
                'title' => 'Themes',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-thumb-tack',
                'color' => '#000000',
                'parent_id' => NULL,
                'order' => 16,
                'created_at' => '2019-05-06 07:31:46',
                'updated_at' => '2019-05-06 20:18:45',
                'route' => 'voyager.themes.index',
                'parameters' => 'null',
            ),
        ));


    }
}
