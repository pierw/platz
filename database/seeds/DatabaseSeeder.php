<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(ThemesTableSeeder::class);

        $this->call(PostsTableSeeder::class);

        $this->call(MenusTableCustomizationsSeeder::class);
        $this->call(MenuItemsTableCustomizationsSeeder::class);
        $this->call(DataTypesTableCustomizationsSeeder::class);
        $this->call(DataRowsTableCustomizationsSeeder::class);
        $this->call(PermissionsTableCustomizedSeeder::class);
        $this->call(PermissionRoleTableCustomizedSeeder::class);
    }
}
