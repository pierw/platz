<?php

use Illuminate\Database\Seeder;

class DataTypesTableCustomizationsSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {

        \DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        \DB::table('data_types')->delete();
        \DB::statement('SET FOREIGN_KEY_CHECKS=1;');


        \DB::table('data_types')->insert(array (
            0 =>
            array (
                'id' => 1,
                'name' => 'users',
                'slug' => 'users',
                'display_name_singular' => 'User',
                'display_name_plural' => 'Users',
                'icon' => 'voyager-person',
                'model_name' => 'TCG\\Voyager\\Models\\User',
                'policy_name' => 'TCG\\Voyager\\Policies\\UserPolicy',
                'controller' => 'TCG\\Voyager\\Http\\Controllers\\VoyagerUserController',
                'description' => '',
                'generate_permissions' => 1,
                'server_side' => 0,
                'details' => NULL,
                'created_at' => '2019-05-04 17:06:34',
                'updated_at' => '2019-05-04 17:06:34',
            ),
            1 =>
            array (
                'id' => 2,
                'name' => 'menus',
                'slug' => 'menus',
                'display_name_singular' => 'Menu',
                'display_name_plural' => 'Menus',
                'icon' => 'voyager-list',
                'model_name' => 'TCG\\Voyager\\Models\\Menu',
                'policy_name' => NULL,
                'controller' => '',
                'description' => '',
                'generate_permissions' => 1,
                'server_side' => 0,
                'details' => NULL,
                'created_at' => '2019-05-04 17:06:34',
                'updated_at' => '2019-05-04 17:06:34',
            ),
            2 =>
            array (
                'id' => 3,
                'name' => 'roles',
                'slug' => 'roles',
                'display_name_singular' => 'Role',
                'display_name_plural' => 'Roles',
                'icon' => 'voyager-lock',
                'model_name' => 'TCG\\Voyager\\Models\\Role',
                'policy_name' => NULL,
                'controller' => '',
                'description' => '',
                'generate_permissions' => 1,
                'server_side' => 0,
                'details' => NULL,
                'created_at' => '2019-05-04 17:06:34',
                'updated_at' => '2019-05-04 17:06:34',
            ),
            3 =>
            array (
                'id' => 4,
                'name' => 'posts',
                'slug' => 'posts',
                'display_name_singular' => 'Post',
                'display_name_plural' => 'Posts',
                'icon' => 'voyager-news',
                'model_name' => 'App\\Post',
                'policy_name' => NULL,
                'controller' => NULL,
                'description' => NULL,
                'generate_permissions' => 1,
                'server_side' => 0,
                'details' => '{"order_column":null,"order_display_column":null,"order_direction":"desc","default_search_key":"name","scope":null}',
                'created_at' => '2019-05-06 07:28:01',
                'updated_at' => '2019-05-08 15:24:40',
            ),
            4 =>
            array (
                'id' => 5,
                'name' => 'themes',
                'slug' => 'themes',
                'display_name_singular' => 'Theme',
                'display_name_plural' => 'Themes',
                'icon' => NULL,
                'model_name' => 'App\\Theme',
                'policy_name' => NULL,
                'controller' => NULL,
                'description' => NULL,
                'generate_permissions' => 1,
                'server_side' => 0,
                'details' => '{"order_column":"id","order_display_column":"image","order_direction":"asc","default_search_key":"name","scope":null}',
                'created_at' => '2019-05-06 07:31:46',
                'updated_at' => '2019-05-09 18:24:24',
            ),
        ));


    }
}
