<?php

use Illuminate\Database\Seeder;

class PermissionsTableCustomizedSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {

        \DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        \DB::table('permissions')->delete();
        \DB::statement('SET FOREIGN_KEY_CHECKS=1;');


        \DB::table('permissions')->insert(array (
            0 =>
            array (
                'id' => 1,
                'key' => 'browse_admin',
                'table_name' => NULL,
                'created_at' => '2019-05-04 17:06:34',
                'updated_at' => '2019-05-04 17:06:34',
            ),
            1 =>
            array (
                'id' => 2,
                'key' => 'browse_bread',
                'table_name' => NULL,
                'created_at' => '2019-05-04 17:06:34',
                'updated_at' => '2019-05-04 17:06:34',
            ),
            2 =>
            array (
                'id' => 3,
                'key' => 'browse_database',
                'table_name' => NULL,
                'created_at' => '2019-05-04 17:06:34',
                'updated_at' => '2019-05-04 17:06:34',
            ),
            3 =>
            array (
                'id' => 4,
                'key' => 'browse_media',
                'table_name' => NULL,
                'created_at' => '2019-05-04 17:06:34',
                'updated_at' => '2019-05-04 17:06:34',
            ),
            4 =>
            array (
                'id' => 5,
                'key' => 'browse_compass',
                'table_name' => NULL,
                'created_at' => '2019-05-04 17:06:34',
                'updated_at' => '2019-05-04 17:06:34',
            ),
            5 =>
            array (
                'id' => 6,
                'key' => 'browse_menus',
                'table_name' => 'menus',
                'created_at' => '2019-05-04 17:06:34',
                'updated_at' => '2019-05-04 17:06:34',
            ),
            6 =>
            array (
                'id' => 7,
                'key' => 'read_menus',
                'table_name' => 'menus',
                'created_at' => '2019-05-04 17:06:34',
                'updated_at' => '2019-05-04 17:06:34',
            ),
            7 =>
            array (
                'id' => 8,
                'key' => 'edit_menus',
                'table_name' => 'menus',
                'created_at' => '2019-05-04 17:06:34',
                'updated_at' => '2019-05-04 17:06:34',
            ),
            8 =>
            array (
                'id' => 9,
                'key' => 'add_menus',
                'table_name' => 'menus',
                'created_at' => '2019-05-04 17:06:34',
                'updated_at' => '2019-05-04 17:06:34',
            ),
            9 =>
            array (
                'id' => 10,
                'key' => 'delete_menus',
                'table_name' => 'menus',
                'created_at' => '2019-05-04 17:06:34',
                'updated_at' => '2019-05-04 17:06:34',
            ),
            10 =>
            array (
                'id' => 11,
                'key' => 'browse_roles',
                'table_name' => 'roles',
                'created_at' => '2019-05-04 17:06:34',
                'updated_at' => '2019-05-04 17:06:34',
            ),
            11 =>
            array (
                'id' => 12,
                'key' => 'read_roles',
                'table_name' => 'roles',
                'created_at' => '2019-05-04 17:06:34',
                'updated_at' => '2019-05-04 17:06:34',
            ),
            12 =>
            array (
                'id' => 13,
                'key' => 'edit_roles',
                'table_name' => 'roles',
                'created_at' => '2019-05-04 17:06:34',
                'updated_at' => '2019-05-04 17:06:34',
            ),
            13 =>
            array (
                'id' => 14,
                'key' => 'add_roles',
                'table_name' => 'roles',
                'created_at' => '2019-05-04 17:06:34',
                'updated_at' => '2019-05-04 17:06:34',
            ),
            14 =>
            array (
                'id' => 15,
                'key' => 'delete_roles',
                'table_name' => 'roles',
                'created_at' => '2019-05-04 17:06:34',
                'updated_at' => '2019-05-04 17:06:34',
            ),
            15 =>
            array (
                'id' => 16,
                'key' => 'browse_users',
                'table_name' => 'users',
                'created_at' => '2019-05-04 17:06:34',
                'updated_at' => '2019-05-04 17:06:34',
            ),
            16 =>
            array (
                'id' => 17,
                'key' => 'read_users',
                'table_name' => 'users',
                'created_at' => '2019-05-04 17:06:34',
                'updated_at' => '2019-05-04 17:06:34',
            ),
            17 =>
            array (
                'id' => 18,
                'key' => 'edit_users',
                'table_name' => 'users',
                'created_at' => '2019-05-04 17:06:34',
                'updated_at' => '2019-05-04 17:06:34',
            ),
            18 =>
            array (
                'id' => 19,
                'key' => 'add_users',
                'table_name' => 'users',
                'created_at' => '2019-05-04 17:06:34',
                'updated_at' => '2019-05-04 17:06:34',
            ),
            19 =>
            array (
                'id' => 20,
                'key' => 'delete_users',
                'table_name' => 'users',
                'created_at' => '2019-05-04 17:06:34',
                'updated_at' => '2019-05-04 17:06:34',
            ),
            20 =>
            array (
                'id' => 21,
                'key' => 'browse_settings',
                'table_name' => 'settings',
                'created_at' => '2019-05-04 17:06:34',
                'updated_at' => '2019-05-04 17:06:34',
            ),
            21 =>
            array (
                'id' => 22,
                'key' => 'read_settings',
                'table_name' => 'settings',
                'created_at' => '2019-05-04 17:06:34',
                'updated_at' => '2019-05-04 17:06:34',
            ),
            22 =>
            array (
                'id' => 23,
                'key' => 'edit_settings',
                'table_name' => 'settings',
                'created_at' => '2019-05-04 17:06:34',
                'updated_at' => '2019-05-04 17:06:34',
            ),
            23 =>
            array (
                'id' => 24,
                'key' => 'add_settings',
                'table_name' => 'settings',
                'created_at' => '2019-05-04 17:06:34',
                'updated_at' => '2019-05-04 17:06:34',
            ),
            24 =>
            array (
                'id' => 25,
                'key' => 'delete_settings',
                'table_name' => 'settings',
                'created_at' => '2019-05-04 17:06:34',
                'updated_at' => '2019-05-04 17:06:34',
            ),
            25 =>
            array (
                'id' => 26,
                'key' => 'browse_hooks',
                'table_name' => NULL,
                'created_at' => '2019-05-04 17:06:34',
                'updated_at' => '2019-05-04 17:06:34',
            ),
            26 =>
            array (
                'id' => 27,
                'key' => 'browse_posts',
                'table_name' => 'posts',
                'created_at' => '2019-05-06 07:28:01',
                'updated_at' => '2019-05-06 07:28:01',
            ),
            27 =>
            array (
                'id' => 28,
                'key' => 'read_posts',
                'table_name' => 'posts',
                'created_at' => '2019-05-06 07:28:01',
                'updated_at' => '2019-05-06 07:28:01',
            ),
            28 =>
            array (
                'id' => 29,
                'key' => 'edit_posts',
                'table_name' => 'posts',
                'created_at' => '2019-05-06 07:28:01',
                'updated_at' => '2019-05-06 07:28:01',
            ),
            29 =>
            array (
                'id' => 30,
                'key' => 'add_posts',
                'table_name' => 'posts',
                'created_at' => '2019-05-06 07:28:01',
                'updated_at' => '2019-05-06 07:28:01',
            ),
            30 =>
            array (
                'id' => 31,
                'key' => 'delete_posts',
                'table_name' => 'posts',
                'created_at' => '2019-05-06 07:28:01',
                'updated_at' => '2019-05-06 07:28:01',
            ),
            31 =>
            array (
                'id' => 32,
                'key' => 'browse_themes',
                'table_name' => 'themes',
                'created_at' => '2019-05-06 07:31:46',
                'updated_at' => '2019-05-06 07:31:46',
            ),
            32 =>
            array (
                'id' => 33,
                'key' => 'read_themes',
                'table_name' => 'themes',
                'created_at' => '2019-05-06 07:31:46',
                'updated_at' => '2019-05-06 07:31:46',
            ),
            33 =>
            array (
                'id' => 34,
                'key' => 'edit_themes',
                'table_name' => 'themes',
                'created_at' => '2019-05-06 07:31:46',
                'updated_at' => '2019-05-06 07:31:46',
            ),
            34 =>
            array (
                'id' => 35,
                'key' => 'add_themes',
                'table_name' => 'themes',
                'created_at' => '2019-05-06 07:31:46',
                'updated_at' => '2019-05-06 07:31:46',
            ),
            35 =>
            array (
                'id' => 36,
                'key' => 'delete_themes',
                'table_name' => 'themes',
                'created_at' => '2019-05-06 07:31:46',
                'updated_at' => '2019-05-06 07:31:46',
            ),
        ));


    }
}
