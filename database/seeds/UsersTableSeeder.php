<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        \DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        \DB::table('users')->delete();
        \DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        \DB::table('users')->insert(array (
            0 =>
            array (
                'id' => 1,
                'role_id' => 1,
                'name' => 'mathieu',
                'email' => 'mathieu.henrotay@gmail.com',
                'avatar' => 'users\\May2019\\ZrjKWv75wAZPt07kURVZ.jpg',
                'email_verified_at' => NULL,
                'password' => '$2y$10$NLljBlKZ8wAwS7A4IZnVzuIXpnuf4MqC0hJfH.3eork6UAHsd1EtW',
                'remember_token' => NULL,
                'settings' => '{"locale":"en"}',
                'created_at' => '2019-05-03 10:36:35',
                'updated_at' => '2019-05-03 11:01:43',
            ),
            1 =>
            array (
                'id' => 2,
                'role_id' => 1,
                'name' => 'pierre',
                'email' => 'pierre.wasilewski.webdev@gmail.com',
                'avatar' => 'users/default.png',
                'email_verified_at' => NULL,
                'password' => '$2y$10$JHt5Z3bSwW7Vqd4tYUFGmOupyHOjlZqY/dAutTz/zPwegTD5Bc6VO',
                'remember_token' => NULL,
                'settings' => '{"locale":"en"}',
                'created_at' => '2019-05-03 10:55:03',
                'updated_at' => '2019-05-03 10:55:03',
            ),
            2 =>
            array (
                'id' => 3,
                'role_id' => 1,
                'name' => 'admin',
                'email' => 'admin@admin.com',
                'avatar' => 'users/default.png',
                'email_verified_at' => NULL,
                'password' => '$2y$10$QDex6lBG4P08lsiqYc4rW.3.fJNkzXD5nibQM3mu1B/AmmHxrnYQm',
                'remember_token' => 'Xo3hK2HOjaZw5LJR2VFOh00YOhvUkoBiVESsM33vBAIr7vlcAe8fTBVhhR3U',
                'settings' => '{"locale":"en"}',
                'created_at' => '2019-05-06 07:20:26',
                'updated_at' => '2019-05-06 07:20:26',
            ),
        ));


    }
}
