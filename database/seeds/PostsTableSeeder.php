<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Post;

class PostsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {

/*        \DB::table('posts')->insert(array (
            0 =>
            array (
                'id' => 1,
                'name' => 'eaaezaezazaez',
                'image' => 'posts\\May2019\\PpnJuF0SUtIYfWvISFco.jpg',
                'text' => '<p>eazaezazeazeaez</p>',
                'file' => '[{"download_link":"posts\\\\May2019\\\\2Qyeopre7XGHj5HlnuWe.svg","original_name":"icon-download.svg"}]',
                'theme_id' => 1,
                'user_id' => 2,
                'created_at' => '2019-05-04 12:16:40',
                'updated_at' => '2019-05-04 12:16:40',
            ),
        ));*/
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        Post::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $count = (int)$this->command->ask('How many entries do you need ?', 100);

        $posts = factory(Post::class, $count)->create();

        foreach ($posts as $post) {
          $post->slug = str_replace(' ', '-', $post->name);
          $post->save();
        }

        $this->command->info('entries Created!');


    }
}
