<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePostsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('posts', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name', 100)->unique('name_UNIQUE');
			$table->string('image', 191);
			$table->text('text', 65535)->nullable();
			$table->string('file', 191)->nullable();
			$table->integer('theme_id')->unsigned()->index('fk_posts_themes1_idx');
			$table->bigInteger('user_id')->unsigned()->nullable()->index('fk_posts_users1_idx');
			$table->timestamps();
			$table->string('slug', 191)->unique();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('posts');
	}

}
