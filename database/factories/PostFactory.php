<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Post;
use Faker\Generator as Faker;

$factory->define(Post::class, function (Faker $faker) {
    return [
      'name'=>$faker->unique()->words(2, true),
      'slug'=>$faker->unique()->word(),
      'image'=>'posts\May2019\\'.$faker->randomElement(["theme-4.jpg", "ai-1.jpg", "ai-2.jpg", "font-1.jpg", "font-2.jpg", "font-3.jpg", "font-4.jpg", "font-5.jpg", "font-6.jpg", "icons-1.jpg", "icons-2.jpg", "icons-3.jpg", "mockup-1.jpg", "mockup-2.jpg", "mockup-3.jpg", "psd-1.jpg", "psd-2.jpg", "psd-3.jpg", "psd-4.jpg", "psd-5.jpg", "psd-6.jpg", "theme-1.jpg", "theme-2.jpg", "theme-3.jpg", ]),
      'text'=>$faker->text(300),
      'file'=>'[{"download_link":"posts\\\\May2019\\\\HtYvZNu0LBWiu8dF6XHQ.psd","original_name":"lavitz_theme.psd"}]',
      /*'theme_id'=>$faker->numberBetween(1,6),*/
      'theme_id'=>function () {
            // Get random theme id
            return App\Theme::inRandomOrder()->first()->id;
        },
      /*'user_id'=>$faker->numberBetween(1,3),*/
      'user_id'=>function () {
            // Get random genre id
            return App\User::inRandomOrder()->first()->id;
        }
    ];
});
