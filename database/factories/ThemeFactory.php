<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\User;
use App\Theme;
use Faker\Generator as Faker;

$factory->define(Theme::class, function (Faker $faker) {
    return [
        /*'image' => $faker->randomElement([
          '[{"download_link":"themes\\\\May2019\\\\zz3ze2K400KsNv74sjDg.svg","original_name":"icon-psd.svg"}]', '[{"download_link":"themes\\\\May2019\\\\HnG66TbTIFkdPT8VARfZ.svg","original_name":"icon-ai.svg"}]', '[{"download_link":"themes\\\\May2019\\\\yguBlH7uB0BlO5SuT3pG.svg","original_name":"icon-font.svg"}]',
          '[{"download_link":"themes\\\\May2019\\\\fuVggrjyACqi2TtsFSs1.svg","original_name":"icon-themes.svg"}]',
          '[{"download_link":"themes\\\\May2019\\\\97EPvOkx1ITZ5eqy98fs.svg","original_name":"icon-photo.svg"}]',
          '[{"download_link":"themes\\\\May2019\\\\64rS1OXWQ88jaiQGCep0.svg","original_name":"icon-premium.svg"}]'
        ]),*/
        'image' => 'aaaaa',
        'name' => $faker->unique()->word(),
        /*'color' => strtoupper($faker->unique()->hexcolor())*/
        'user_id' => function(){
          return User::inRandomOrder()->first()->id;
        }
    ];
});
